<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BarcodeController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request)
    {
        return $this->render('default/index.html.twig');
    }
    #[Route('/generate_barcode', name: 'generate_barcode', methods: ['POST'])]
    public function generateBarcode(Request $request): JsonResponse
    {
        $text = $request->request->get('text');

        // Tutaj generuj kod kreskowy dla tekstu
        $barcode = $this->generateBarcodeForText($text);

        return $this->json(['barcode' => $barcode]);
    }

    private function generateBarcodeForText(string $text): string
    {
        $barcode = new \SimpleSoftwareIO\QrCode\Generator\BaconQrCodeGenerator;
        return base64_encode($barcode->size(300)->generate($text));
    }
}
