$(document).ready(function() {
    $('#generate-barcode-btn').click(function() {
        var textInput = $('#text-input').val();

        // Wyślij żądanie do kontrolera BarcodeController w celu wygenerowania kodu kreskowego
        $.ajax({
            url: '/generate_barcode',
            type: 'POST', // Upewnij się, że to jest metoda POST
            data: { text: textInput },
            success: function(response) {
                // Wyświetl kod kreskowy
                $('#barcode-img').attr('src', 'data:image/png;base64,' + response.barcode);
                $('#barcode-display').show();
            },
            error: function() {
                alert('Error generating barcode. Please try again.');
            }
        });
    });
});